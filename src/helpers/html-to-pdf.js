import puppeteer from 'puppeteer';
import fs from 'fs';

const defaultOptions = {
    format: 'A4',
    path: `./uploads/${new Date().toISOString().replace(/:/g, '-').replace(/\..+/, '').replace('T', '_')}.pdf`,
    printBackground: true,
    landscape: true, 
    width: '11in', 
    height: '8.5in', 
};

async function htmlToPdf(htmlFilePath, options = defaultOptions) {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    
    const htmlContent = await fs.promises.readFile(htmlFilePath, 'utf-8');

    await page.setContent(htmlContent, { waitUntil: 'domcontentloaded' });
    
    const pdfBuffer = await page.pdf(options);

    await browser.close();

    return pdfBuffer;
}

export default htmlToPdf;
