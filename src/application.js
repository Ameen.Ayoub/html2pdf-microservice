import express from 'express';
import conversionsController from './controllers/conversions.js'
const PORT = process.env.PORT | 5001;
class Application {
    static async main() {

       const app = express()

       app.get('/', (req,res) => {
        res.send('Hello World!')
       })
       app.use('/convert', conversionsController)
       app.listen(PORT, (req,res) => {
        console.log(`Running on port ${PORT}..`)
       })
}}

export default Application;