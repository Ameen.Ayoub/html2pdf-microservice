import express from 'express';
import htmlToPdf from '../helpers/html-to-pdf.js';
import multer from 'multer';
import path from 'path';
import fs from 'fs';

const router = express.Router();
const upload = multer({ dest: 'uploads/' });

router.post('/convert', upload.single('htmlFile'), async (req, res) => {
    try {

        if (!req.file) {
            return res.status(400).send('No file uploaded.');
        }

        const htmlFilePath = req.file.path; 
        const pdf = await htmlToPdf(htmlFilePath);

        res.contentType('application/pdf');

        res.send(pdf);

    } catch (error) {

        console.error('Error converting HTML to PDF:', error);
        res.status(500).send('Error converting HTML to PDF');
    }
});

export default router;
